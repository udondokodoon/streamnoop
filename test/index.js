var noop = require('../src');
var assert = require('power-assert'),
    path   = require('path'),
    through = require('through2'),
    fs     = require('fs');

var Stream = require('stream');

describe('noopstream', function() {
    var file = path.join(__dirname, 'test.txt');
    beforeEach(function() {
        fs.writeFileSync(file, 'hogehoge');
    });
    it('はreadableストリームに何も影響を与えない', function() {
        var expected = fs.readFileSync(file).toString('utf8');
        console.log(expected);
        var actual = '';
        fs.createReadStream(file)
            .pipe(noop())
            .pipe(through.obj(function(buffer, err, next) {
                actual += buffer.toString('utf8');
                this.push(buffer);
                next();
            }))
            .pipe(through.obj(function(buffer, err, next) {
                assert(actual === expected);
                this.push(buffer);
                next();
            }));
    });
});
